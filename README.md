# Trabajo Practico Nro. 3

# Introducción
En esta unidad nos concentramos en la comunicación de procesos a través de un espacio de memoria compartida. Este informe muestra la resolución de un ejercicio en el cual, creamos una key con el comando ftok, creamos un espacio de memoria compartida, inicializamos un puntero a este espacio y 2 procesos escriben y leen en este espacio sin superponerse al enviarse señales para leer y escribir con el comando “kill”. Para esto ambos guardaron su PID en un lugar del espacio de memoria y leyeron el PID del otro para poder comunicarse.

# Enunciado
Escriba un programa que utilice un arreglo de 20 int, compartido entre dos procesos A y B de manera tal que A escriba los primeros 10, y B los lea solamente después que se completó la escritura. De la misma manera B escribirá los 10 últimos enteros del arreglo y A los leerá solamente después que B complete la escritura. 


Este proceso se repetirá hasta que el Proceso A reciba la señal SIGTERM, y cuando la reciba debe a su vez hacer terminar el proceso B, y liberar todos los recursos compartidos utilizados.

Los datos serán números entre 0 y 26, (representando letras entre la a y la z), que serán cifrados previo a la escritura mediante una función de cifrado de la forma f(x) = (ax + b) mod n. En donde (a,b) serán claves públicas que estarán disponibles en memoria compartida. Para el descifrado de los datos se usará la función inversa f-1(x)= (kx+ c) mod n, en donde (k,c) son claves privadas conocidas solo por el proceso lector.
Como función de cifrado se usará f(x) = (4x + 5) mod 27 y para el descifrado f-1(x) = (7x + 19) mod 27

Ambos procesos realizarán primero la escritura de los datos en la zona correspondiente y luego la lectura. Ninguno de los dos procesos puede escribir en su zona hasta que el otro no haya leído los datos, excepto la primera vez.
La escritura en ambos casos se realizará a razón de un valor por segundo, mientras que la lectura se realizará tan rápido como se pueda y se imprimirá en pantalla junto con la identificación del proceso que está leyendo.
El programa debe ser único, es decir el mismo programa puede actuar como proceso A o como proceso B, dependiendo de un parámetro que se pase en la línea de comando. (por ejemplo “programa a” o “programa b”). Realice su implementación basada en memoria compartida.


# Interpretación
En este ejercicio, el objetivo es que se logre leer y escribir con dos procesos diferentes en un único vector. Algunos de los detalles más importantes son:
 - El ejecutable será el mismo para los dos procesos, se le deberá pasar el argumento **A** o **B** para determinarlo.
 - El que inicia primero, será el encargado de esperar al segundo proceso. Cuando los dos están listos, los dos empiezan a funcionar normalmente ya que lograron enlazar sus pid haciendo uso de la memoria compartida.
 - El proceso A escribirá en las primeras 10 posiciones del vector (el otro proceso estará en espera durante este tiempo). Cuando termine, le enviara una señal *SIGUSR1* al proceso B para que comience a leer estos datos.
 - Una vez que el proceso B termine de escribir, hará el mismo procedimiento, le enviara al proceso A la señal *SIGUSR1*.
 - Cuando reciban la señal *SIGUSR1*, estará en condiciones de leer la posición del vector donde el otro proceso escribió.
 - Finalmente, cuando el proceso A reciba la señal *SIGTERM*, finalizara al proceso B, eliminara la memoria compartida y finalizara el proceso A.
  
  

# Modo de Uso

El Script fue escrito en lenguaje C por tanto deberá primero compilarse:

    gcc -o Ejecutable EjFinal.c
Luego, en una terminal ejecutar:

    ./Ejecutable A
En este punto, este proceso esperara a que se cree el proceso B, por tanto, en otra terminal, ejecutar:

    ./Ejecutable B

(Es indistinto cual de los se ejecuta primero)

Cuando se ejecute el proceso B, ya se habrán leído los pid de cada uno y empezaran el proceso de escritura y lectura tal como se describe en la introducción.
Para finalizar, enviar la señal **SIGTERM** al proceso A:

    kill -SIGTERM 8750
Sera necesario conocer el PID del proceso A, una forma es leerlo directamente de la terminal donde se ejecutó tal proceso ya que aparecerá la leyenda: `[8750] PROCESO A Iniciado!...`, la otra forma es utilizando el comando `ps -uf` que mostrara  el árbol de procesos del sistema.