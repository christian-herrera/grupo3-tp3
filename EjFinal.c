#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>


/* -------------------- Constantes -------------------- */
#define NUMERO 123
#define ARCHIVO "/bin/more"
#define OFFSET 10


/* -------------------- Variables Globales -------------------- */
int leer=0, escribir=0, flag3=0, flag4=0;

struct structDatos{
	int vector[2*OFFSET];
	pid_t pidA, pidB;
	int a, b;
};
typedef struct structDatos stDatos;

/* -------------------- Definiciones de Funciones -------------------- */
void pausa (int signal);
void Pleer(int sig);
void Pescribir(int sig);
void terminar(int sig);

int cifrar(int val, int a, int b);
int descifrar(int val);

void printVector(int * vector, int index);
void writeVector(char * from, int * to, int a, int b);




/* ======================== MAIN ======================== */
int main( int argc, char **argv) {
	key_t Clave;
	int Id_Memoria1;
	long *Memoria1 = NULL;
    struct shmid_ds buf;
	int i,j;

	// Configuro las señales que utilizare
	signal(SIGUSR1,Pleer);
	signal(SIGUSR2,Pescribir);
	signal(SIGCONT,pausa);
	signal(SIGTERM,terminar);

	//Genero una clave para un segmento de memoria compartida
	Clave = ftok (ARCHIVO, NUMERO);
	if (Clave == -1) {
		printf("No consegui  clave para memoria compartida\n");
		exit(1);
	}

    //Obtengo un identificador para un segmento de memoria compartida
    Id_Memoria1 = shmget (Clave, sizeof(struct structDatos), 0666 | IPC_CREAT);
	if (Id_Memoria1 == -1) {
		printf("No consegui Id para memoria compartida\n");
		exit (2);
	}





	// =================================================================> PROCESO A
	if(strcmp(argv[1],"a")==0 || strcmp(argv[1],"A")==0){
		printf("Soy el Proceso A\n");
		
		//Accedo a los datos
		stDatos * datos;
		datos = (stDatos *)shmat(Id_Memoria1, (const void *)0, 0);

		//Leo el pidB y guardo el pidA
		int a = 4;
		int b = 5;
		datos->a = a;
		datos->b = b;
		pid_t pidB = datos->pidB;
		datos->pidA = getpid();
		
		if(pidB == 0) { //B no se creo
			pause(); //Espero a que se cree el proceso B, y leo su PID
			pidB = datos->pidB;
		} else {
			kill(pidB, SIGCONT); //Saco al B de la pausa
		}

        printf("[%d] PROCESO A Iniciado!...\n", getpid());
				
		while(1){
			
			//Escritura (Proc A)
			char palabra[] = "hola";
			writeVector(palabra, datos->vector, a, b);

			//SIGUSR1  -->  Proc B (Terminé de escribir)
			kill(pidB,SIGUSR1);
			
			//Espero a que llege SIGTERM o la señal
			//de que el Proc B ya escribio.
			while(leer==0 && !flag4){
				sleep(1);
			}
			leer=0;

			//Imprimo lo que el Proc B escribió
			printVector(datos->vector, OFFSET);
			
			//SIGUSR2 --> Proc B (Terminé de Leer)
			kill(pidB,SIGUSR2);
			
			//Espero a que llege SIGTERM o la señal
			//de que el Proc B ya leyó
			while(escribir==0 && !flag4){
				sleep(1);
			}
			escribir=0;

			//Si llega SIGTERM, entonces finalizo el Proc B, elimino
			//la memoria compartida, limpio todo y finalizo el Proc A.
			if(flag4==1){
				printf("================> Señal SIGTERM Recibida...\n");
				printf("Terminado Proceso B...\n");
				kill(pidB,SIGTERM);

				printf("Liberando espacio de memoria y terminando.\n");
				//Elimino el attach
				shmdt ((const void *) Memoria1);

				//Libera el espacio de memoria cuando se deja de usar (block)
				shmctl (Id_Memoria1, IPC_RMID, (struct shmid_ds *)NULL);

				return 0;
			}
		}
		
		
		
		




	// =================================================================> PROCESO B
	}else if(strcmp(argv[1],"b")==0 || strcmp(argv[1],"B")==0){
		printf("Soy el Proceso B\n");

		//Accedo a los datos
		stDatos * datos;
		datos = (stDatos *)shmat(Id_Memoria1, (const void *)0, 0);

		//Leo el pidA y guardo el pidB
		pid_t pidA = datos->pidA;
		datos->pidB = getpid();

		if(pidA == 0) { //A no se creo
			pause(); //Espero a que se cree, y leo su PID
			pidA = datos->pidA;
		} else {
			kill(pidA, SIGCONT); //Saco al A de la pausa
		}

		int a = datos->a;
		int b = datos->b;

        printf("[%d] PROCESO B Iniciado!...\n", getpid());
		
		while(1){
			//Escritura (Proc B)
			char palabra[]="chau";
			writeVector(palabra, datos->vector + OFFSET, a, b);

			//SIGUSR1 --> Proc A (Termine de Escribir)
			kill(pidA,SIGUSR1);
			
			//Espero a que llege SIGTERM o la señal
			//de que el Proc A ya escribio.			
			while(leer==0 && !flag4){
				sleep(1);
			}
			leer=0;

			//Imprimo lo que el Proc A escribió
			printVector(datos->vector, 0);

			//SIGUSR2 --> Proc B (Terminé de Leer)
			kill(pidA,SIGUSR2);

			//Espero a que llege SIGTERM o la señal
			//de que el Proc B ya leyó
			while(escribir==0 && !flag4){
				sleep(1);
			}
			escribir=0;

			//Si llega SIGTERM, entonces elimino la memoria compartida
			//y finalizo este proceso (Proc B).
			if(flag4==1){
				printf("================> Señal SIGTERM Recibida...\n");
				printf("Liberando el puntero y terminando.\n");
				//Elimino el attach
				shmdt ((const void *) Memoria1);

				return 0;
			}
		}

	}else{
		printf("o no introdujo nada o introdujo mal a o b\n");
	}

}





/* -------------------- Manejadores de Señales -------------------- */
void pausa(int sig){
	flag3 = 1;
}

void Pleer(int sig){
	leer = 1;
}

void Pescribir(int sig){
	escribir=1;
}

void terminar(int sig){
	flag4=1;
}


/* -------------------- Cifrado y Descifrado -------------------- */
int cifrar(int val, int a, int b){
	return( (a * val + b) % 27 );
}

int descifrar(int val){
	return( (7 * val + 19) % 27 );
}




/* -------------------- Impresion del Vector -------------------- */
// Parametros:
//  - vector: 	Recibira un array donde estara almacenado el vector
//				a imprimir. Para el ejercicio, sera el vector de la
//				memoria compartida.
//	- index:	Sera la posicion de donde se inicia la lectura. Esto
//				es asi porque para el ejercio, los dos procesos usan
//				el mismo vector, en posiciones distintas donde escribir.
void printVector(int * vector, int index){
	printf("Vector Leido = \033[33m[");

	//Leo los valores, los descifro y los imprimo
	for(int i = index; i < index + OFFSET; i++){
        if(i < (index + OFFSET - 1) && vector[i+1] == -1){
            printf("%c",  descifrar(vector[i]) + 97); 
            break;
        }
		printf("%c, ",  descifrar(vector[i]) + 97); 
	}

	printf("]\033[m [");
	
	//Leo los valores raw
    for(int i = index; i < (index + OFFSET - 1); i++){
		printf("%d, ",  vector[i]); 
	}

    printf("%d]\n", vector[index + OFFSET - 1]);
}


/* -------------------- Escritura del Vector -------------------- */
// Parametros:
//	- from:		Es el vector de tipo char de donde se extraen los valores.
//	- to:		Es el vector donde se copiara la informacion cifrada.
void writeVector(char * from, int * to, int a, int b){
	printf("Escribiendo.");

	int cond = 0;

	for(int i = 0; i < OFFSET; i++){
		//Si llege al ultimo elemento del vector
		//entonces, coloco (-1).
		if(from[i] == '\0'){
			cond = 1;
		}

		if(cond){
			to[i] = -1;
		} else {
			to[i] = cifrar(from[i] - 97, a, b);
		}

		sleep(1);
        printf(".");
        fflush(stdout);
	}

    printf("\n");
}